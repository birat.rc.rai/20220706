# 20220706

Chase coding challenge

## Getting started
Feature included
- Display a list of NYC High Schools from the api.
- Display details about the school when selected with additional information.

## App Architecture
- The App has been architectured using MVVM
- Hilt Dependency Injection framework has been used.
- Kotlin and Coroutines (for asynchronous work) has been used.
- Clean Coding has been implemented with separation of concerns.
- Comments has been added to understand the code more easily.


## Unit Test
- Few unit test has been included for different classes.
- Junit and mockito framework has been used to do unit test.


