package com.nyc.school.network

import junit.framework.TestCase
import org.mockito.Mock
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import org.mockito.Mockito

class SchoolServiceTest : TestCase() {
    // Mock objects
    @Mock
    lateinit var schoolApi: SchoolApi

    // Subject under test
    private lateinit var schoolService: SchoolService
    @Before
    public override fun setUp() {
        MockitoAnnotations.openMocks(this)
        schoolService = SchoolService(schoolApi)
    }

    @Test
    fun testGetSchool() {
        // Act
        schoolService.school

        // Assert
        Mockito.verify(schoolApi)?.school
    }

    fun testSearchSchool() {
        // Act
        schoolService.searchSchool("cli")

        // Assert
        Mockito.verify(schoolApi)?.searchSchool("cli")
    }

    fun testGetSatScores() {
        // Act
        schoolService.getSatScores("schoolId")

        // Assert
        Mockito.verify(schoolApi)?.getSatScores("schoolId")
    }
}