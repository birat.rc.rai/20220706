package com.nyc.school.network

import com.nyc.school.data.HighSchools
import com.nyc.school.data.Result
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

class SchoolRepositoryImplTest {
    // Mock objects
    @Mock
    lateinit var schoolService: SchoolService

    @Mock
    lateinit var call: Call<List<HighSchools>>

    @Mock
    lateinit var highSchools: HighSchools

    // Subject under test
    private lateinit var schoolRepository: SchoolRepository
    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Mockito.`when`(schoolService.school).thenReturn(call)
        schoolRepository = SchoolRepositoryImpl(schoolService)
    }

    @Test
    @Throws(IOException::class)
    fun testGetSchoolFetchesSchoolGivesCorrectData() {
        // Arrange
        val response = Response.success(listOf(highSchools))
        Mockito.`when`(call.execute()).thenReturn(response)

        // Act
        val result = schoolRepository.getSchool()

        // Assert
        Mockito.verify(schoolService).school
        Assert.assertTrue(result is Result.Success<*>)
    }

    @Test
    @Throws(IOException::class)
    fun testGetSchoolFetchesSchoolThrowsException() {
        // Arrange
        Mockito.`when`(call.execute()).thenThrow(
            IOException::class.java
        )

        // Act
        val result: Result<List<HighSchools>> = schoolRepository.getSchool()

        // Assert
        Mockito.verify(schoolService).school
        Assert.assertTrue(result is Result.Error<*>)
        Assert.assertTrue((result as Result.Error<*>).exception is IOException)
    }
}