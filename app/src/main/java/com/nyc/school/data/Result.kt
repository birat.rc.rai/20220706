package com.nyc.school.data

import java.lang.Exception

abstract class Result<T> private constructor() {
    class Success<T>(var data: T) : Result<T>()
    class Error<T>(var exception: Exception) : Result<T>()
}