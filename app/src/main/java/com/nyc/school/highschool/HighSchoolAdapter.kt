package com.nyc.school.highschool

import com.nyc.school.data.HighSchools
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import com.nyc.school.R
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.Filter
import android.widget.Filterable
import java.util.*
import kotlin.collections.ArrayList

class HighSchoolAdapter(
    private var filteredHighSchoolsList: MutableList<HighSchools>,
    itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<HighSchoolAdapter.ViewHolder>(), Filterable {
    private lateinit var highSchoolsList: List<HighSchools>
    private val itemClickListener: OnItemClickListener

    fun setData(schools: MutableList<HighSchools>) {
        filteredHighSchoolsList = schools
        highSchoolsList = ArrayList(schools)
        notifyDataSetChanged()
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val schoolNameTextView: TextView
        private val schoolLocationTextView: TextView
        internal fun bind(clickListener: OnItemClickListener, highSchool: HighSchools) {
            itemView.setOnClickListener {
                highSchool.dbn?.let { id ->
                    clickListener.onItemClicked(
                        id
                    )
                }
            }
            schoolNameTextView.text = highSchool.schoolName
            schoolLocationTextView.text = highSchool.city
        }

        init {
            schoolNameTextView = itemView.findViewById(R.id.high_school_name)
            schoolLocationTextView = itemView.findViewById(R.id.high_school_location)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.high_school_item, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.bind(itemClickListener, filteredHighSchoolsList[position])
    }

    // Return the size of your data (invoked by the layout manager)
    override fun getItemCount(): Int {
        return filteredHighSchoolsList.size
    }

    override fun getFilter(): Filter {
        return searchedFilter
    }

    private val searchedFilter: Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence): FilterResults {
            val filteredList = ArrayList<HighSchools>()
            // If charSequence is null or empty show all highSchools
            if (constraint.isEmpty()) {
                filteredList.addAll(highSchoolsList)
            } else {
                // If charSequence is valid search for related school name
                val filterPattern =
                    constraint.toString().lowercase(Locale.getDefault()).trim { it <= ' ' }
                for (item in highSchoolsList) {
                    if (item.schoolName?.lowercase(Locale.getDefault())?.contains(filterPattern) == true) {
                        filteredList.add(item)
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            filteredHighSchoolsList.clear()
            filteredHighSchoolsList.addAll(results.values as Collection<HighSchools>)
            notifyDataSetChanged()
        }
    }

    // Interface for ItemClick on the adapter
    interface OnItemClickListener {
        fun onItemClicked(highSchoolDbn: String)
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param schools List of high schools.
     * @param itemClickListener interface for click listener
     */
    init {
        highSchoolsList = ArrayList(filteredHighSchoolsList)
        this.itemClickListener = itemClickListener
    }
}