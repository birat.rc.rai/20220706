package com.nyc.school.highschool

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nyc.school.R
import com.nyc.school.data.HighSchools
import com.nyc.school.data.Result
import com.nyc.school.databinding.ActivityHighSchoolBinding
import com.nyc.school.satscore.SatScoreActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HighSchoolActivity : AppCompatActivity(), HighSchoolAdapter.OnItemClickListener {
    private val highSchoolViewModel: HighSchoolViewModel by viewModels()
    private lateinit var highSchoolAdapter: HighSchoolAdapter
    private lateinit var activityHighSchoolBinding: ActivityHighSchoolBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityHighSchoolBinding = ActivityHighSchoolBinding.inflate(layoutInflater)

        setContentView(activityHighSchoolBinding.root)

        initializeViews()

        initializeData()
    }

    private fun initializeData() {
        highSchoolViewModel.highSchools.observe(this) { highSchools -> setHighSchoolData(highSchools) }
        highSchoolViewModel.fetchSchools()
    }

    private fun setHighSchoolData(highSchools: Result<List<HighSchools>>?) {
        if (highSchools is Result.Success) {
            highSchoolAdapter.setData(highSchools.data.toMutableList())
        } else if (highSchools is Result.Error || highSchools == null) {
            activityHighSchoolBinding.errorMessage.visibility = View.VISIBLE
        }
        activityHighSchoolBinding.progressBar.visibility = View.GONE
    }

    private fun initializeViews() {
        val recyclerView = findViewById<RecyclerView>(R.id.high_school_recycler_view)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        highSchoolAdapter = HighSchoolAdapter(mutableListOf(), this)
        recyclerView.adapter = highSchoolAdapter

        // textQuery listener
        activityHighSchoolBinding.searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(searchString: String): Boolean {
                handleSearch(searchString)
                return false
            }

            override fun onQueryTextChange(searchString: String): Boolean {
                handleSearch(searchString)
                return false
            }
        })
    }

    private fun handleSearch(searchString: String) {
        highSchoolAdapter.filter.filter(searchString)
    }

    override fun onItemClicked(highSchoolDbn: String) {
        val intent = Intent(this, SatScoreActivity::class.java)
        intent.putExtra(HIGH_SCHOOL_DBN, highSchoolDbn)
        startActivity(intent)
    }

    companion object {
        const val HIGH_SCHOOL_DBN = "Sat_Score"
    }
}