package com.nyc.school.satscore

import dagger.hilt.android.AndroidEntryPoint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.nyc.school.data.SatScore
import android.view.View
import com.nyc.school.highschool.HighSchoolActivity
import android.widget.TextView
import androidx.activity.viewModels
import com.nyc.school.R
import com.nyc.school.data.Result
import com.nyc.school.databinding.ActivitySatScoreBinding

@AndroidEntryPoint
class SatScoreActivity : AppCompatActivity() {
    private val satScoreViewModel: SatScoreViewModel by viewModels()
    private lateinit var satScoreBinding: ActivitySatScoreBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        satScoreBinding = ActivitySatScoreBinding.inflate(layoutInflater)
        setContentView(satScoreBinding.root)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initializeData()
    }

    private fun initializeData() {
        satScoreViewModel.satScore.observe(this) { satResult: Result<SatScore>? ->
            handleData(
                satResult
            )
        }

        // Get the Intent that started this activity and extract the string
        val intent = intent
        val highSchoolDbn = intent.getStringExtra(HighSchoolActivity.HIGH_SCHOOL_DBN)

        // Fetch sat score com.nyc.school.data
        satScoreViewModel.fetchSatScore(highSchoolDbn!!)
    }

    private fun handleData(satResult: Result<SatScore>?) {
        satScoreBinding.progressBar.visibility = View.GONE
        if (satResult is Result.Success<*>) {
            setData(satResult as Result.Success<SatScore>)
        } else if (satResult is Result.Error<*> || satResult == null) {
            satScoreBinding.errorMessage.visibility = View.VISIBLE
        }
    }

    private fun setData(satResult: Result.Success<SatScore>) {
        val title = findViewById<TextView>(R.id.title)
        title.text = "Average Sat score of " + satResult.data.schoolName
        val mathScore = findViewById<TextView>(R.id.math_score)
        mathScore.text = "Math average score: " + satResult.data.satMathAvgScore
        mathScore.visibility = View.VISIBLE
        val writingScore = findViewById<TextView>(R.id.writing_score)
        writingScore.text = "Writing average score: " + satResult.data.satWritingAvgScore
        writingScore.visibility = View.VISIBLE
        val readingScore = findViewById<TextView>(R.id.reading_score)
        readingScore.text = "Reading average score: " + satResult.data.satCriticalReadingAvgScore
        readingScore.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}