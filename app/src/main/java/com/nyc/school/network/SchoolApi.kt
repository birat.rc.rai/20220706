package com.nyc.school.network

import retrofit2.http.GET
import com.nyc.school.data.HighSchools
import com.nyc.school.data.SatScore
import retrofit2.Call
import retrofit2.http.Query

interface SchoolApi {
    @get:GET("s3k6-pzi2.json")
    val school: Call<List<HighSchools>>

    @GET("s3k6-pzi2.json")
    fun searchSchool(@Query("source") searchString: String?): Call<List<HighSchools>>

    @GET("f9bf-2cp4.json")
    fun getSatScores(@Query("dbn") schoolId: String?): Call<List<SatScore>>
}