package com.nyc.school.network

import com.nyc.school.data.HighSchools
import com.nyc.school.data.Result
import com.nyc.school.data.SatScore

interface SchoolRepository {

    fun getSchool(): Result<List<HighSchools>>

    fun searchSchool(searchString: String): Result<List<HighSchools>>

    fun getSatScores(schoolId: String): Result<SatScore>
}