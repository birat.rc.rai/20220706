package com.nyc.school.network

import android.util.Log
import javax.inject.Inject
import com.nyc.school.data.HighSchools
import com.nyc.school.data.Result
import com.nyc.school.data.SatScore
import java.io.IOException
import java.lang.Exception

class SchoolRepositoryImpl @Inject internal constructor(private val schoolService: SchoolService) :
    SchoolRepository {
    override fun getSchool(): Result<List<HighSchools>> {
        val result: Result<List<HighSchools>> = try {
            val schools = schoolService.school.execute().body()
            Log.d(TAG, "getSchool() $schools")
            if (schools != null && schools.isNotEmpty()) {
                Result.Success(schools)
            } else {
                Result.Error(Exception("No data available"))
            }
        } catch (e: IOException) {
            Log.e(TAG, "Exception while fetching high school list $e")
            Result.Error(e)
        }
        return result
    }

    override fun searchSchool(searchString: String): Result<List<HighSchools>> {
        val result: Result<List<HighSchools>> = try {
            val schools = schoolService.searchSchool(searchString).execute().body()
            if (schools != null && schools.isNotEmpty()) {
                Result.Success(schools)
            } else {
                Result.Error(Exception("No data available"))
            }
        } catch (e: IOException) {
            Log.e(TAG, "Exception while fetching high school list $e")
            Result.Error(e)
        }
        return result
    }

    override fun getSatScores(schoolId: String): Result<SatScore> {
        val result: Result<SatScore> = try {
            val call = schoolService.getSatScores(schoolId)
            val results = call.execute().body()
            Log.d(TAG, "The satResult $results")
            if (results != null && results.isNotEmpty()) {
                Result.Success(results[0])
            } else {
                Result.Error(Exception("No data available"))
            }
        } catch (e: IOException) {
            Log.e(TAG, "Exception occurred while fetching sat score ", e)
            Result.Error(e)
        }
        return result
    }

    companion object {
        val TAG: String = SchoolRepositoryImpl::class.java.simpleName
    }
}