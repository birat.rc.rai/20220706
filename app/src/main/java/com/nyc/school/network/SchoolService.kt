package com.nyc.school.network

import javax.inject.Singleton
import javax.inject.Inject
import com.nyc.school.data.HighSchools
import com.nyc.school.data.SatScore
import retrofit2.Call

@Singleton
open class SchoolService @Inject constructor(private val schoolApi: SchoolApi) : SchoolApi {
    override val school: Call<List<HighSchools>>
        get() = schoolApi.school

    override fun searchSchool(searchString: String?): Call<List<HighSchools>> {
        return schoolApi.searchSchool(searchString)
    }

    override fun getSatScores(schoolId: String?): Call<List<SatScore>> {
        return schoolApi.getSatScores(schoolId)
    }
}